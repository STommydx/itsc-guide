Contribute to the Guide
========================

The guide is currently maintained by the leaders and some volunteed helpers. We appreciate any input from you all to make the guide better.

Editing the Guide
------------------

Prerequisites
~~~~~~~~~~~~~

You will need a GitLab Account for editing the docs.

Using the GitLab Editor
~~~~~~~~~~~~~~~~~~~~~~~

After clicking ``Edit on GitLab`` on the top-right corner of the page. You will get into the corresponding GitLab page of the guide. Click ``Edit`` to edit the document.